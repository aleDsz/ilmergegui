﻿namespace ILMergeGui
{
    using System;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Reflection;
    using System.Windows.Forms;

    /// <summary>
    /// About Dialog.
    /// </summary>
    public partial class AboutDialog : Form
    {
        /// <summary>
        /// The picasadownloader at bitbucket.
        /// </summary>
        public const String bitbucket = "https://wvd-vegt.bitbucket.io/";

        /// <summary>
        /// The il merge graphical user interface mailto.
        /// </summary>
        const String ILMergeGUI_mailto = "mailto:wim@vander-vegt.nl?SUBJECT=Suggestion for ILMergeGui";

        /// <summary>
        /// The il merge graphical user interface paypal.
        /// </summary>
        const String ILMergeGUI_paypal = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=84Y4JSJE47R7J";

        /// <summary>
        /// Constructor.
        /// </summary>
        public AboutDialog()
        {
            InitializeComponent();

            textBox1.Text = String.Format(textBox1.Text, Assembly.GetExecutingAssembly().GetName().Version);
        }

        /// <summary>
        /// Event handler. Called by pictureBox1 for click events.
        /// </summary>
        ///
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Event information. </param>
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start(ILMergeGUI_paypal);
            }
            catch (Win32Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        /// <summary>
        /// Event handler. Called by linkLabel1 for link clicked events.
        /// </summary>
        ///
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Link label link clicked event information. </param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(bitbucket);
            }
            catch (Win32Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        /// <summary>
        /// Event handler. Called by linkLabel2 for link clicked events.
        /// </summary>
        ///
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Link label link clicked event information. </param>
        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Process.Start(ILMergeGUI_mailto);
            }
            catch (Win32Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }
    }
}
